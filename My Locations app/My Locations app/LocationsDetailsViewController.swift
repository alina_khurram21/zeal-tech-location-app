//
//  LocationsDetailsViewController.swift
//  My Locations app
//
//  Created by Syed Daniyal Khurram on 26/02/2021.
//

import UIKit
class LocationDetailsViewController: UITableViewController {
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBAction func done() {
        navigationController?.popViewController(animated: true)
    }
    
   @IBAction func cancel() {
        navigationController?.popViewController(animated: true)
    }
}
